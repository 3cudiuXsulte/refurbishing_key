#!/bin/bash

# parted.sh --
#
#   This file allows the automatic reconditionning of a computer
#   by automating the installation with Clonezilla
#
#   Created on 2010-23 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was tested and validated on Clonezilla live 3.0.1-8 i686/amd64
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

DRIVE=/dev/sda
UEFI_ON=0
NUMBER_PART_EXTEND=0

DRIVE=/dev/${1}
UEFI_ON=${2}
NAME_CLONE=${3}
NUMBER_PART_EXTEND=${4}

YELLOW='\033[1;33m'
GREEN='\033[0;32m'
RED='\033[0;31m'
ORANGE='\033[0;33m'
NC='\033[0m' # No Color

#DEBUG="true"
#BEEP_OFF="true"

LANGUE=$(echo ${LANG} | cut -d _ -f1)


RAM_Mo=4000

# Activation du pavé numérique
setleds -D +num

if [ ${LANGUE} = fr ]
then
    message_nom_disque_cible="Disque cible à partitionner :"
    message_numero_partition_etendre="Numéro de la partition Linux à étendre :"
    message_format_partition_inconnu="Erreur : Format de la partition inconnue :"
    message_nombre_partition_linux="Nombre de partitions Linux supérieur à 1."
    message_saisir_numero_partition_linux="Veuillez indiquer le numéro de la partition à étendre : "
    message_nombre="Saisissez un nombre ci-dessus, puis validez : "
    message_numero_partition_linux="Erreur : Numéro de la partition Linux à étendre doit être inférieur ou égal au nombre de partitions Linux présentes :"
    message_partition_linux_non_trouve="Erreur : Partition Linux introuvable"
    message_disque_cible_trop_petit="Erreur : Taille du disque cible trop petite par rapport à celle du clone"
    message_creation_partition_bios="Création en mode BIOS de la partition numéro :"
    message_arret_script="Les partitions du disque dur ne seront pas modifiées à cause de l'erreur ci-dessus.\nAppuyez sur la touche \"Entrée\" pour arrêter le script."
    message_pause_script="Tapez \"Entrée\" pour continuer ou CTRL+C pour arrêter"

else
    message_nom_disque_cible="Target disk to partition :"
    message_numero_partition_etendre="Linux partition number to extend :"
    message_format_partition_inconnu="Error : Unknown partition format :"
    message_nombre_partition_linux="Number of Linux partition greater than 1."
    message_saisir_numero_partition_linux="Please indicate the number of the partition to be extended : "
    message_nombre="Enter a number above, then validate: "
    message_numero_partition_linux="Error: Number of the Linux partition to extend must be less than or equal to the number of Linux partitions present:"
    message_partition_linux_non_trouve="Error : Partition Linux not found"
    message_disque_cible_trop_petit="Error: Target disk size too small compared to clone size"
    message_creation_partition_bios="Creation in BIOS mode of partition number :"
    message_arret_script="Hard drive partitions will not be modified due to the above error.\nPress the \"Enter\" key to stop the script."
    message_pause_script="Type \"Enter\" to continue or CTRL+C to stop"

fi

# Info sur les paramètres d'entrée du script
echo ""

echo -e "${message_nom_disque_cible} ${GREEN}${DRIVE}${NC}"
echo -e "Clone : ${GREEN}${NAME_CLONE}${NC}"

if [ ${UEFI_ON} -eq 1  ] ; then
    echo -e "Mode UEFI : ${GREEN}on${NC}"
else
    echo -e "Mode UEFI : ${GREEN}off${NC}"
fi

if [[ ${NUMBER_PART_EXTEND} == "" ]] ; then
    NUMBER_PART_EXTEND=0
elif [ ${NUMBER_PART_EXTEND} -eq 0 ] ; then
    NUMBER_PART_EXTEND=0
else
    echo -e "${message_numero_partition_etendre} ${GREEN}${NUMBER_PART_EXTEND}${NC}"
fi

echo ""

mem_info=$(sudo dmesg | grep Memory)
RAM_Ko=$(echo ${mem_info} | sed 's/[^M]*Memory:[^/]*\/\([^K]*\)[^$]*/\1/')

if [ $RAM_Ko -le 612000 ]; then
RAM_Mo=1000
elif [ $RAM_Ko -le 1124000 ]; then
RAM_Mo=2000
elif [ $RAM_Ko -le 2148000 ]; then
RAM_Mo=4000
else
RAM_Mo=8000
fi


# Détermination du nombre et des types de partitions du clone
NAME_DISK_CLONE=$(cat ${NAME_CLONE}/disk)
MODE_EXTEND=0

SIZE_DISK_CLONE=$(cat ${NAME_CLONE}/blkdev.list | grep ${NAME_DISK_CLONE} | grep "disk")
SIZE_DISK_CLONE=$(echo ${SIZE_DISK_CLONE} | sed s/${NAME_DISK_CLONE}//g)
SIZE_DISK_CLONE=$(echo ${SIZE_DISK_CLONE} | cut -d " " -f1)

# Vérification si le clone a été créé avec un disque dur nvme
# Si oui ajout du suffixe "p"
if [[ $(echo ${NAME_DISK_CLONE} | grep "nvme") != "" ]] ; then

    NAME_DISK_CLONE="${NAME_DISK_CLONE}p"

fi


function find_type_part()
{

    number_part=${1}

    type_part=$(cat ${NAME_CLONE}/dev-fs.list | grep ${NAME_DISK_CLONE}${number_part} | cut -d " " -f2)

    if [[ ${type_part} == ext* ]] ; then

        type_part_sfdisk="L"

    elif [ ${type_part} = "swap" ] ; then

        type_part_sfdisk="S"

    elif [ ${type_part} = "vfat" ] ; then

        type_part_sfdisk="U"

    elif [ ${type_part} = "bios_grub" ] ; then

        type_part_sfdisk=""

    elif [ ${type_part} = "btrfs" ] || [ ${type_part} = "xfs" ] || [ ${type_part} = "f2fs" ] ; then

        type_part_sfdisk="L"

    else

        type_part_sfdisk="Error ${type_part}"

    fi

    #echo "type_part = ${type_part}, type_part_sfdisk = ${type_part_sfdisk}"

    echo ${type_part_sfdisk}

}

function find_size_part()
{

    number_part=${1}
    plus_un=0

    size_part=$(cat ${NAME_CLONE}/blkdev.list | grep ${NAME_DISK_CLONE}${1} | sed -e "s/[[:space:]]\+/ /g" | cut -d " " -f3 | grep "M\|G")

    # Supprime la partie décimale et ajoute +1
    if [ $(echo ${size_part} | grep "\.") ] ; then
        size_part=$(echo ${size_part} | sed -e "s/\..//")
        plus_un=1
    fi

    if [ $(echo ${size_part} | grep "G") ] ; then
        size_Mo=$(echo ${size_part} | sed s/G//)
        size_Mo=$((${size_Mo} + ${plus_un}))
        size_Mo=$((${size_Mo} * 1024))
    else
        size_Mo=$(echo ${size_part} | sed s/M//)
        size_Mo=$((${size_Mo} + ${plus_un}))
    fi

    #echo "size_part = ${size_part}, size_Mo = ${size_Mo}, plus_un = ${plus_un}"

    echo ${size_Mo}

}


NB_PARTS=$(cat ${NAME_CLONE}/dev-fs.list | grep ${NAME_DISK_CLONE} | cut -d " " -f1 | wc -w)

PART_LINUX=$(cat ${NAME_CLONE}/dev-fs.list | grep ${NAME_DISK_CLONE} | grep ext | cut -d " " -f1)
# Supprime /dev/ dans le nom des partition
PART_LINUX=$(echo ${PART_LINUX} | sed s/\\/dev\\///g)
# Tri les partitions dans le bon ordre
PART_LINUX=$(echo ${PART_LINUX} | tr " " "\n" | sort -n | tr "\n" " ")
NB_PARTS_LINUX=$(echo ${PART_LINUX} | wc -w)

NUMBER_PART=$(cat ${NAME_CLONE}/dev-fs.list | grep ${NAME_DISK_CLONE} | cut -d " " -f1)
# Supprime /dev/disk dans le nom des partition
NUMBER_PART=$(echo ${NUMBER_PART} | sed s/\\/dev\\/${NAME_DISK_CLONE}//g)
# Tri les partitions dans le bon ordre
NUMBER_PART=$(echo ${NUMBER_PART} | tr " " "\n" | sort -n | tr "\n" " ")

NB_PARTS_OTHER_FORMAT=$(cat ${NAME_CLONE}/dev-fs.list | grep ${NAME_DISK_CLONE} | grep "btrfs\|xfs\|f2fs" | cut -d " " -f1 | wc -w)

# Erreur de déterminantion du nombre ou du numéro de partition
if [[ -z ${NB_PARTS} ]] || [[ -z ${NUMBER_PART} ]] ; then

    echo -e "${RED}Error : NB_PARTS = ${NB_PARTS}, NUMBER_PART = ${NUMBER_PART}${NC}"
    echo -e "${RED}${message_arret_script}${NC}"
    read pause
    exit 1

fi

# Nombre de partitions Linux supérieur à 1
if [ ${NB_PARTS_LINUX} -gt 1 ] || [ ${NB_PARTS_OTHER_FORMAT} -gt 1 ] ; then

    # Renseignement du NUMBER_PART_EXTEND si nul, sinon renseigné par le mode automatique
    if [ ${NUMBER_PART_EXTEND} -eq 0 ]  ; then

        if [ -z ${BEEP_OFF} ] ; then
            echo -ne "\a"
        fi

        PART_LINUX_1=$(echo ${PART_LINUX} | cut -d " " -f1)
        PART_LINUX_2=$(echo ${PART_LINUX} | cut -d " " -f2)
        echo -e "${ORANGE}${message_nombre_partition_linux}${NC}"
        echo -e "${message_saisir_numero_partition_linux}"
        echo -e "${YELLOW}1${NC} - ${GREEN}${PART_LINUX_1}${NC}"
        echo -e "${YELLOW}2${NC} - ${GREEN}${PART_LINUX_2}${NC}"
        echo -ne "${YELLOW}${message_nombre}${NC}${ORANGE}"
        read NUMBER_PART_EXTEND
        echo -ne "${NC}"
    fi

fi

# Vérification du numéro de la partition Linux à étendre
if [ ${NUMBER_PART_EXTEND} -gt ${NB_PARTS_LINUX}  ] ; then
    echo -e "${RED}${message_numero_partition_linux} ${NB_PARTS_LINUX}${NC}"
    echo -e "${RED}${message_arret_script}${NC}"
    read pause
    exit 2
fi

# Vérification que la taille du disque cible est au moins égale à celle du disque du clone
SIZE_DISK_TARGET_Ko=$(sudo sfdisk -s ${DRIVE})
SIZE_DISK_TARGET_Mo=$(echo "$SIZE_DISK_TARGET_Ko/1024" |bc)
SIZE_DISK_CLONE_Mo=$(find_size_part ${SIZE_DISK_CLONE})

if [ "${SIZE_DISK_TARGET_Mo}" -lt "${SIZE_DISK_CLONE_Mo}" ] ; then

    echo -e "${RED}${message_disque_cible_trop_petit}${NC}"
    echo -e "${RED}${message_arret_script}${NC}"
    read pause
    exit 3

fi

# Initialisation de la taille de la partition Linux à 0 pour faire la comparaison
size_part_linux=0
order_part_linux=1
index_part_id=1
index_part_id_extend=1
for part_id in ${NUMBER_PART}
do

    type_part=$(find_type_part ${part_id})
    size_part=$(find_size_part ${part_id})

    if [[ $(echo ${type_part} | grep "Error") != "" ]] ; then

        type_part=$(echo ${type_part} | cut -d " " -f2)

        echo -e "${RED}${message_format_partition_inconnu} ${type_part}${NC}"
        echo -e "${RED}${message_arret_script}${NC}"
        read pause
        exit 4

    fi

    # Si mode BIOS
    if [ ${UEFI_ON} -eq 0 ] ; then

        if [ ${part_id} -gt ${NB_PARTS} ] ; then

            if [ ${MODE_EXTEND} -eq 0 ] ; then
                part_id_extend=${index_part_id}
                SIZE_PART[${part_id_extend}]=""
                TYPE_PART[${part_id_extend}]="Ex"
                MODE_EXTEND=1
            fi

            part_id=$((${part_id_extend}+${index_part_id_extend}))
            index_part_id_extend=$((${index_part_id_extend}+1))
       fi

    fi

    # Swap changement de la taille de la partition déterminée par la taille de la RAM
    if [ "${type_part}" = "S" ] ; then

        number_part_swap=${part_id}
        size_part=${RAM_Mo}

    fi

    # Détermination de la partition Root de type Linux aka L qui a la plus grande taille
    if [ "${type_part}" = "L" ] ; then

        if [ "${size_part}" -gt "${size_part_linux}" ] ; then
            if  [ ${NUMBER_PART_EXTEND} -eq 0 ] || [ ${order_part_linux} -eq  ${NUMBER_PART_EXTEND} ] ; then
                number_part_linux=${part_id}
                size_part_linux=${size_part}
            fi
            order_part_linux=$(($order_part_linux+1))
        fi

    fi

    SIZE_PART[${part_id}]="${size_part}"
    TYPE_PART[${part_id}]="${type_part}"

    index_part_id=$((${index_part_id}+1))

    if [ ! -z ${DEBUG} ] ; then
       echo "part_id=${part_id}"
       echo "size_part=${size_part}"
       echo "type_part=${type_part}"
    fi

 done


if [ ${UEFI_ON} -eq 1 ] ; then

    TYPE_FORMAT_DISK=gpt

    if [ ! -z ${DEBUG} ] ; then
        echo -e "${YELLOW}UEFI on - NUMBER_PART =  ${NB_PARTS}${NC}"
    fi

else

    TYPE_FORMAT_DISK=dos

    if [ ! -z ${DEBUG} ] ; then
        echo -e "${YELLOW}UEFI off - NUMBER_PART =  ${NB_PARTS}${NC}"
    fi

fi

NB_PARTS=$((${NB_PARTS}+${MODE_EXTEND}))

# Calcul de la taille de la partition root aka Linux
# qui est égal à la taille du disque moins la taille des autres partitions sauf la partion Linux et la partition étendue

size_part_linux=${SIZE_DISK_TARGET_Mo}
size_part_linux_clone=0

if [ ! -z ${number_part_linux} ] ; then

    for (( part_id = 1 ; part_id <= ${NB_PARTS} ; part_id++ ))
    do

        if [ "${part_id}" != "${number_part_linux}" ] && [ "${TYPE_PART[${part_id}]}" != "Ex" ] ; then

            size_part_linux=$((${size_part_linux}-${SIZE_PART[${part_id}]}))

        elif [ "${part_id}" = "${number_part_linux}" ] ; then

            size_part_linux_clone=${SIZE_PART[${part_id}]}

        fi
    done

    # Test si la taille de la partition root est inférieure à la taille de la partition root définie par le clone
    # Dans ce cas affecte la taille de la partition root à celle définie dans le clone, et recalcule la taille de la swap
    if [ "${size_part_linux}" -lt "${size_part_linux_clone}" ] ; then

        size_part_linux=${size_part_linux_clone}
        SIZE_PART[${number_part_linux}]=${size_part_linux_clone}

        if [ ! -z ${number_part_swap} ] ; then

            size_part_swap=${SIZE_DISK_TARGET_Mo}
            for (( part_id = 1 ; part_id <= ${NB_PARTS} ; part_id++ ))
            do

                if [ "${part_id}" != "${number_part_swap}" ] && [ "${TYPE_PART[${part_id}]}" != "Ex" ] ; then

                    size_part_swap=$((${size_part_swap}-${SIZE_PART[${part_id}]}))

                fi
            done

            SIZE_PART[${number_part_swap}]="${size_part_swap}"

        fi

    else

        SIZE_PART[${number_part_linux}]="${size_part_linux}"

    fi

    # Ajout du suffixe M au taille en Mo
    for (( part_id = 1 ; part_id <= ${NB_PARTS} ; part_id++ ))
    do

        if [ "${TYPE_PART[${part_id}]}" != "Ex" ] ; then

            SIZE_PART[${part_id}]="${SIZE_PART[${part_id}]}M"

        fi
    done


else

    echo -e "${RED}${message_partition_linux_non_trouve}${NC}"
    echo -e "${RED}${message_arret_script}${NC}"
    read pause
    exit 5

fi

NUMBER_PART_EXTEND=${NB_PARTS}

if [ ! -z ${DEBUG} ] ; then

    for (( part_id = 1 ; part_id <= ${NUMBER_PART_EXTEND} ; part_id++ ))
    do
        echo -e "${YELLOW}SIZE_PART[${part_id}] = ${SIZE_PART[${part_id}]}, TYPE_PART[${part_id}]= ${TYPE_PART[${part_id}]}${NC}"

    done

fi

if [ ! -z ${DEBUG} ] ; then

    echo ""
    echo -e "${ORANGE}${message_pause_script}${NC}"
    read pause

fi


if [ ${NB_PARTS} -eq 1 ] ; then

    {
    echo label: ${TYPE_FORMAT_DISK}
    echo ,+,L,*
    } | sudo sfdisk --wipe always ${DRIVE}

elif [ ${NB_PARTS} -eq 2 ] ; then

    {
    echo label: ${TYPE_FORMAT_DISK}
    echo ,${SIZE_PART[1]},${TYPE_PART[1]},*
    echo ,+,${TYPE_PART[2]}
    } | sudo sfdisk --wipe always ${DRIVE}

elif [ ${NB_PARTS} -eq 3 ] ; then

    {
    echo label: ${TYPE_FORMAT_DISK}
    echo ,${SIZE_PART[1]},${TYPE_PART[1]},*
    echo ,${SIZE_PART[2]},${TYPE_PART[2]}
    echo ,+,${TYPE_PART[3]}
    } | sudo sfdisk --wipe always ${DRIVE}

elif [ ${NB_PARTS} -eq 4 ] ; then

    {
    echo label: ${TYPE_FORMAT_DISK}
    echo ,${SIZE_PART[1]},${TYPE_PART[1]},*
    echo ,${SIZE_PART[2]},${TYPE_PART[2]}
    echo ,${SIZE_PART[3]},${TYPE_PART[3]}
    echo ,+,${TYPE_PART[4]}
    } | sudo sfdisk --wipe always ${DRIVE}

elif [ ${NB_PARTS} -eq 5 ] ; then

    {
    echo label: ${TYPE_FORMAT_DISK}
    echo ,${SIZE_PART[1]},${TYPE_PART[1]},*
    echo ,${SIZE_PART[2]},${TYPE_PART[2]}
    echo ,${SIZE_PART[3]},${TYPE_PART[3]}
    echo ,${SIZE_PART[4]},${TYPE_PART[4]}
    echo ,+,${TYPE_PART[5]}
    } | sudo sfdisk --wipe always ${DRIVE}

fi

NUMBER_PART_BIOS_GRUB=$(cat ${NAME_CLONE}/dev-fs.list | grep ${NAME_DISK_CLONE} | grep "bios_grub" | cut -d " " -f1)
NUMBER_PART_BIOS_GRUB=$(echo ${NUMBER_PART_BIOS_GRUB} | sed s/\\/dev\\/${NAME_DISK_CLONE}//g)

# Création d'une partition BIOS_GRUB
if [ ! -z ${NUMBER_PART_BIOS_GRUB} ] ; then

    echo -e "${YELLOW}${message_creation_partition_bios} ${NUMBER_PART_BIOS_GRUB}${NC}"

    sudo parted ${DRIVE} set ${NUMBER_PART_BIOS_GRUB} bios on

    sudo sfdisk -l ${DRIVE}

fi

if [ ! -z ${DEBUG} ] ; then

    echo ""
    echo -e "${ORANGE}${message_pause_script}${NC}"
    read pause

fi
